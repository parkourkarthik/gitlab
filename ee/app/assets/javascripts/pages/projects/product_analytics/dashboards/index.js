import mountDashboards from 'ee/analytics/analytics_dashboards/mount_dashboards';
import AnalyticsApp from 'ee/product_analytics/product_analytics_app.vue';

mountDashboards('js-analytics-dashboard', AnalyticsApp);
